import os
import random
import sys

import pygame

size = width, height = 600, 95
screen = pygame.display.set_mode(size)

pygame.display.set_caption('Машинка')
clock = pygame.time.Clock()


def load_image(name, colorkey=None):
    fullname = os.path.join('data', name)
    # если файл не существует, то выходим
    if not os.path.isfile(fullname):
        sys.exit()
    image = pygame.image.load(fullname)
    if colorkey is not None:
        image = image.convert()
        if colorkey == -1:
            colorkey = image.get_at((0, 0))
        image.set_colorkey(colorkey)
    else:
        image = image.convert_alpha()
    return image


class Car(pygame.sprite.Sprite):
    image = load_image("car2.png")

    def __init__(self, *group):
        super().__init__(*group)
        self.image = Car.image
        self.rect = self.image.get_rect()
        self.rect.x = 0
        self.rect.y = 0
        self.vx = 10

    def update(self, *args):
        if self.rect.x == 450:
            self.vx = -self.vx
            self.image = pygame.transform.flip(Car.image, True, False)
        elif self.rect.x == -10:
            self.vx = -self.vx
            self.image = Car.image
        self.rect.x += self.vx


all_sprites = pygame.sprite.Group()

Car(all_sprites)

running = True
while running:
    clock.tick(30)
    all_sprites.update()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.MOUSEBUTTONDOWN:
            all_sprites.update(event)

    screen.fill(pygame.Color("white"))
    all_sprites.draw(screen)
    pygame.display.flip()

pygame.quit()
